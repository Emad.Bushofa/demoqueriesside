using Grpc.Core;

namespace DemoQueriesSide.Services
{
    public class EmployeesService : Employees.EmployeesBase
    {
        public override Task<GetSingleResponse> GetSingle(GetSingleRequest request, ServerCallContext context)
        {
            return base.GetSingle(request, context);
        }

        public override Task<FilterResponse> Filter(FilterRequest request, ServerCallContext context)
        {
            return base.Filter(request, context);
        }
    }
}

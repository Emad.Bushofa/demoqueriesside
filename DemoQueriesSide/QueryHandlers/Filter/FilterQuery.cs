﻿using DemoQueriesSide.Entities;
using DemoQueriesSide.Infrastructre.Data;
using MediatR;

namespace DemoQueriesSide.QueryHandlers.Filter
{
    public class FilterQuery : IRequest<IEnumerable<Employee>>
    {
    }

    public class FilterQueryHandler : IRequestHandler<FilterQuery, IEnumerable<Employee>>
    {
        private readonly ApplicationDbContext _context;

        public FilterQueryHandler(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Employee>> Handle(FilterQuery request, CancellationToken cancellationToken)
        {
            throw new Exception("Not implemented");
        }
    }
}

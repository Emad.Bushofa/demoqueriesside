﻿namespace DemoQueriesSide.EventHandlers.InfoChanged
{
    public record EmployeeContactInfoChanged(
        Guid AggregateId,
        int Sequence,
        EmployeeContactInfoChangedData Data,
        DateTime DateTime,
        string UserId,
        int Version
    ) : Event<EmployeeContactInfoChangedData>(
        AggregateId: AggregateId,
        Sequence: Sequence,
        Data: Data,
        DateTime: DateTime,
        UserId: UserId,
        Version: Version
    );
}

﻿namespace DemoQueriesSide.EventHandlers.InfoChanged
{
    public record EmployeeContactInfoChangedData(
        string Phone,
        string Email
    );
}

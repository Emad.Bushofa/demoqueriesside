﻿using DemoQueriesSide.Infrastructre.Data;
using MediatR;

namespace DemoQueriesSide.EventHandlers.InfoChanged
{
    public class EmployeeContactInfoChangedHandler(ApplicationDbContext context, ILogger<EmployeeContactInfoChangedHandler> logger)
        : IRequestHandler<EmployeeContactInfoChanged, bool>
    {
        private readonly ApplicationDbContext _context = context;
        private readonly ILogger<EmployeeContactInfoChangedHandler> _logger = logger;

        public async Task<bool> Handle(EmployeeContactInfoChanged @event, CancellationToken cancellationToken)
        {
            var employee = await _context.Employees.FindAsync(@event.AggregateId);

            if (employee is null)
            {
                _logger.LogWarning("Employee {EmployeeId} not found", @event.AggregateId);
                return false;
            }

            if (@event.Sequence <= employee.Sequence) return true;

            if (@event.Sequence > employee.Sequence + 1)
            {
                _logger.LogWarning("Sequence {Sequence} is not expected for employee {EmployeeId}", @event.Sequence, @event.AggregateId);
                return false;
            }

            employee.ContactInfoChanged(@event);
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}

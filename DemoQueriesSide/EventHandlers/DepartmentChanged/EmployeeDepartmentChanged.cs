﻿namespace DemoQueriesSide.EventHandlers.DepartmentChanged
{
    public record EmployeeDepartmentChanged(
        Guid AggregateId,
        int Sequence,
        EmployeeDepartmentChangedData Data,
        DateTime DateTime,
        string UserId,
        int Version
    ) : Event<EmployeeDepartmentChangedData>(
        AggregateId: AggregateId,
        Sequence: Sequence,
        Data: Data,
        DateTime: DateTime,
        UserId: UserId,
        Version: Version
    );
}

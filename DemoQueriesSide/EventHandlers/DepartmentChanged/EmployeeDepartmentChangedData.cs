﻿namespace DemoQueriesSide.EventHandlers.DepartmentChanged
{
    public record EmployeeDepartmentChangedData(int DepartmentId);
}

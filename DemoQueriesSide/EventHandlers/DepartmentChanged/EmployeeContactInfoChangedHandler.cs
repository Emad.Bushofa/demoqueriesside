﻿using DemoQueriesSide.Infrastructre.Data;
using MediatR;

namespace DemoQueriesSide.EventHandlers.DepartmentChanged
{
    public class EmployeeDepartmentChangedHandler(ApplicationDbContext context, ILogger<EmployeeDepartmentChangedHandler> logger)
        : IRequestHandler<EmployeeDepartmentChanged, bool>
    {
        private readonly ApplicationDbContext _context = context;
        private readonly ILogger<EmployeeDepartmentChangedHandler> _logger = logger;

        public async Task<bool> Handle(EmployeeDepartmentChanged @event, CancellationToken cancellationToken)
        {
            var employee = await _context.Employees.FindAsync(@event.AggregateId);

            if (employee is null)
            {
                _logger.LogWarning("Employee {EmployeeId} not found", @event.AggregateId);
                return false;
            }

            if (@event.Sequence <= employee.Sequence) return true;

            if (@event.Sequence > employee.Sequence + 1)
            {
                _logger.LogWarning("Sequence {Sequence} is not expected for employee {EmployeeId}", @event.Sequence, @event.AggregateId);
                return false;
            }

            employee.DepartmentChanged(@event);
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}

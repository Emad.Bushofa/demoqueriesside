﻿using DemoQueriesSide.Infrastructre.Data;
using MediatR;

namespace DemoQueriesSide.EventHandlers.IncrementSequence
{
    public class UnknownEventHandler(ApplicationDbContext context, ILogger<UnknownEventHandler> logger) : IRequestHandler<UnknownEvent, bool>
    {
        private readonly ApplicationDbContext _context = context;
        private readonly ILogger<UnknownEventHandler> _logger = logger;

        public async Task<bool> Handle(UnknownEvent @event, CancellationToken cancellationToken)
        {
            var employee = await _context.Employees.FindAsync(@event.AggregateId);

            if (employee is null)
            {
                _logger.LogWarning("Employee {EmployeeId} not found", @event.AggregateId);
                return false;
            }

            if (@event.Sequence <= employee.Sequence) return true;

            if (@event.Sequence > employee.Sequence + 1)
            {
                _logger.LogWarning("Sequence {Sequence} is not expected for employee {EmployeeId}", @event.Sequence, @event.AggregateId);
                return false;
            }

            employee.IncrementSequence();
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}

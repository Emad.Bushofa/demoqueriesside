﻿namespace DemoQueriesSide.EventHandlers.IncrementSequence
{
    public record class UnknownEvent(
        Guid AggregateId,
        int Sequence,
        DateTime DateTime,
        string UserId,
        int Version
    ) : Event<object>(
        AggregateId: AggregateId,
        Sequence: Sequence,
        Data: new(),
        DateTime: DateTime,
        UserId: UserId,
        Version: Version
    );
}

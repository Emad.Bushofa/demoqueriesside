﻿namespace DemoQueriesSide.EventHandlers.Registered
{
    public record EmployeeRegisteredData(
        string FirstName,
        string? MiddleName,
        string LastName,
        string Phone,
        string Email,
        int DepartmentId
    );
}

﻿namespace DemoQueriesSide.EventHandlers.Registered
{
    public record class EmployeeRegistered(
        Guid AggregateId,
        int Sequence,
        EmployeeRegisteredData Data,
        DateTime DateTime,
        string UserId,
        int Version
    ) : Event<EmployeeRegisteredData>(
        AggregateId: AggregateId,
        Sequence: Sequence,
        Data: Data,
        DateTime: DateTime,
        UserId: UserId,
        Version: Version
    );
}

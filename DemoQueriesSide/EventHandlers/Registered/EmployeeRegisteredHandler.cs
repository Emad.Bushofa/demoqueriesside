﻿using DemoQueriesSide.Entities;
using DemoQueriesSide.Infrastructre.Data;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace DemoQueriesSide.EventHandlers.Registered
{
    public class EmployeeRegisteredHandler(ApplicationDbContext context, ILogger<EmployeeRegisteredHandler> logger) : IRequestHandler<EmployeeRegistered, bool>
    {
        private readonly ApplicationDbContext _context = context;
        private readonly ILogger<EmployeeRegisteredHandler> _logger = logger;

        public async Task<bool> Handle(EmployeeRegistered @event, CancellationToken cancellationToken)
        {
            if (await _context.Employees.AnyAsync(e => e.Id == @event.AggregateId, cancellationToken))
                return true;

            if (await _context.Departments.AnyAsync(d => d.Id == @event.Data.DepartmentId, cancellationToken))
            {
                _logger.LogWarning("Department {DepartmentId} not found", @event.Data.DepartmentId);
                return false;
            }

            var isDuplicate = await _context.Employees.AnyAsync(e => e.NormalizedPhone == @event.Data.Phone.ToUpperInvariant(), cancellationToken);

            await _context.Employees.AddAsync(Employee.FromRegisteredEvent(@event, isUnique: !isDuplicate), cancellationToken);
            await _context.SaveChangesAsync(cancellationToken);
            return true;
        }
    }
}

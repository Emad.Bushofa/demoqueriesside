﻿using MediatR;

namespace DemoQueriesSide.EventHandlers
{
    public record Event<T>(
        Guid AggregateId,
        int Sequence,
        T Data,
        DateTime DateTime,
        string UserId,
        int Version
    ) : IRequest<bool>;
}

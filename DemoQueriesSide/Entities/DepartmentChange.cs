﻿using DemoQueriesSide.EventHandlers.DepartmentChanged;

namespace DemoQueriesSide.Entities
{
    public class DepartmentChange
    {
        private DepartmentChange(int id, int departmentId, Guid employeeId, DateTime registeredAt)
        {
            Id = id;
            DepartmentId = departmentId;
            EmployeeId = employeeId;
            RegisteredAt = registeredAt;
        }

        public static DepartmentChange FromDepartmentChangedEvent(EmployeeDepartmentChanged @event)
            => new(
                id: 0,
                departmentId: @event.Data.DepartmentId,
                employeeId: @event.AggregateId,
                registeredAt: @event.DateTime
            );

        public int Id { get; private set; }
        public int DepartmentId { get; private set; }
        public Guid EmployeeId { get; private set; }
        public Department? Department { get; private set; }
        public Employee? Employee { get; private set; }
        public DateTime RegisteredAt { get; private set; }
    }
}

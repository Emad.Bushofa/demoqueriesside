﻿namespace DemoQueriesSide.Entities
{
    public class Department
    {
        private Department(int id, string? name, string? location, int sequence)
        {
            Id = id;
            Name = name;
            Location = location;
            Sequence = sequence;
        }

        public int Id { get; private set; }
        public int Sequence { get; private set; }
        public string? Name { get; private set; }
        public string? Location { get; private set; }
        public bool IsDeleted { get; private set; }
    }
}

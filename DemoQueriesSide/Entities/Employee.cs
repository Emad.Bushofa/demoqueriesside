﻿using DemoQueriesSide.EventHandlers.DepartmentChanged;
using DemoQueriesSide.EventHandlers.InfoChanged;
using DemoQueriesSide.EventHandlers.Registered;

namespace DemoQueriesSide.Entities
{
    public class Employee
    {
        private Employee(
            Guid id,
            int sequence,
            string firstName,
            string? middleName,
            string lastName,
            string phone,
            string? normalizedPhone,
            bool isUniquePhone,
            string email,
            int departmentId,
            int totalDepartmentsChanges
        )
        {
            Id = id;
            Sequence = sequence;
            FirstName = firstName;
            MiddleName = middleName;
            LastName = lastName;
            Phone = phone;
            NormalizedPhone = normalizedPhone;
            IsUniquePhone = isUniquePhone;
            Email = email;
            DepartmentId = departmentId;
            TotalDepartmentsChanges = totalDepartmentsChanges;
        }

        public static Employee FromRegisteredEvent(EmployeeRegistered @event, bool isUnique)
            => new(
                id: @event.AggregateId,
                sequence: @event.Sequence,
                firstName: @event.Data.FirstName,
                middleName: @event.Data.MiddleName,
                lastName: @event.Data.LastName,
                phone: @event.Data.Phone,
                normalizedPhone: isUnique ? @event.Data.Phone.ToUpperInvariant() : null,
                isUniquePhone: isUnique,
                email: @event.Data.Email,
                departmentId: @event.Data.DepartmentId,
                totalDepartmentsChanges: 0
            );

        public Guid Id { get; private set; }
        public int Sequence { get; private set; }
        public string FirstName { get; private set; }
        public string? MiddleName { get; private set; }
        public string LastName { get; private set; }
        public string Phone { get; private set; }
        public string? NormalizedPhone { get; private set; }
        public bool IsUniquePhone { get; private set; }
        public string Email { get; private set; }
        public int DepartmentId { get; private set; }
        public Department? Department { get; private set; }
        public int TotalDepartmentsChanges { get; private set; }
        public ICollection<DepartmentChange> DepartmentChanges { get; } = [];

        public void ContactInfoChanged(EmployeeContactInfoChanged @event)
        {
            Sequence = @event.Sequence;
            Phone = @event.Data.Phone;
            Email = @event.Data.Email;
        }

        public void DepartmentChanged(EmployeeDepartmentChanged @event)
        {
            Sequence = @event.Sequence;
            DepartmentId = @event.Data.DepartmentId;
            TotalDepartmentsChanges++;
            DepartmentChanges.Add(DepartmentChange.FromDepartmentChangedEvent(@event));
        }

        public void IncrementSequence() => Sequence++;
    }
}

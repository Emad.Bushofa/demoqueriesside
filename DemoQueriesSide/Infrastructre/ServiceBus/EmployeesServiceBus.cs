﻿using Azure.Messaging.ServiceBus;

namespace DemoQueriesSide.Infrastructre.ServiceBus
{
    public class EmployeesServiceBus(IConfiguration configuration)
    {
        public ServiceBusClient Client { get; } = new ServiceBusClient(configuration["EmployeesServiceBusConnectionString"]);
    }
}

﻿using DemoQueriesSide.Entities;
using DemoQueriesSide.Infrastructre.Data.Configurations;
using Microsoft.EntityFrameworkCore;

namespace DemoQueriesSide.Infrastructre.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new EmployeeConfigurations());
            modelBuilder.ApplyConfiguration(new DepartmentConfigurations());
        }

        public DbSet<Employee> Employees { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<DepartmentChange> DepartmentChanges { get; set; }
    }
}

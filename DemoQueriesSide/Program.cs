using DemoQueriesSide.Infrastructre.Data;
using DemoQueriesSide.Infrastructre.ServiceBus;
using DemoQueriesSide.Services;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddGrpc();
builder.Services.AddMediatR(x => x.RegisterServicesFromAssemblyContaining<Program>());
builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
});
builder.Services.AddSingleton<EmployeesServiceBus>();
builder.Services.AddSingleton<DeparmentsServiceBus>();
builder.Services.AddHostedService<EmployeesListener>();
builder.Services.AddHostedService<DepartmentsListener>();

var app = builder.Build();

// Configure the HTTP request pipeline.
app.MapGrpcService<EmployeesService>();
app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

app.Run();
